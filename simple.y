%{                                                                                    
   #include<stdio.h>
   #include <stdarg.h> 
   #include "simple_shared.h"                                                        

  void yyerror (char const *);
  int yylex();

   int yydebug=1;                                                                    
   int indent=0;                                                                                                                                         
%}     

%union
{
  char * sval;
  int ival;
};

%token TONELINECOMMENTARY
%token TMULTIPLELINECOMMENTARY                         
                                                            
%token TINTEGER 
%token TDOUBLE                                                                           
%token TNUMBER

%token TBOOL
%token TVOID
%token TSTRING

%token TPUBLIC
%token TPRIVATE
%token TPROTECTED

%token TDYNAMIC
%token TVAR

%token TCONSTANT
%token TFINAL

%token TList

%token TASSIGNMENT
%token TNEGATION
%token TSEMICOLON
%token TCOMMA 
%token TDOT
%token TFATARROW

%token TParenthesesOpen
%token TParenthesesClose

%token TBracketsOpen
%token TBracketsClose

%token TOPENINDEX
%token TCLOSEINDEX

%token TCLASS
%token TNEW
%token TEXTENDS

%token TRETURN

%token TIF
%token TELSEIF
%token TELSE

%token TSWICH
%token TCASE

%token TBREAK
%token TCONTINUE

%token TTHIS
%token TSUPER

%token TFOR

%token TWHILE
%token TDO

%token TCOMPARISON
%token TDIFFERENT

%token TLESSTHAN
%token TLESSOREQUALTHAN
%token TGREATERTHAN
%token TGREATEROREQUALTHAN   

%token TASKING
%token TCOLON

%token TAND
%token TOR

%token TADDITION
%token TSUBSTRACTION
%token TDIVISION
%token TMULTIPLICATION

%token <sval> TIDENTIFIER

%token TINTEGERVALUE
%token TDOUBLEVALUE
%token TBOOLVALUE
%token TSTRINGVALUE

%token TCONSOLE
%token TLOG
%token TPUNTO

%token TIMPORT
%% /* Grammar rules and actions follow */

program:        importLibrary {printf("%3d: Importacion de libreria\n", line_number);}
                |importLibrary program  {printf("%3d: Importacion de libreria\n", line_number);}                                     
                |programContent {printf("%3d: Contenido del programa\n", line_number);}
                |programContent program {printf("%3d: Contenido del programa\n", line_number);}
                |classDefinition  {printf("%3d: Definicion de una clase\n", line_number);}            
                |classDefinition program  {printf("%3d: Definicion de una clase\n", line_number);}                                        
                                
programContent: declaration {printf("%3d: Declaracion de una variable\n", line_number);}
                |classInstance  {printf("%3d: Instancia de una clase\n", line_number);}                   
                |execution  {printf("%3d: Ejecucion de una funcion\n", line_number);}                                
                |functionDeclaration  {printf("%3d: Declaracion de una funcion\n", line_number);}
                |comentary  {printf("%3d: Comentario\n", line_number);}              

context:        emptyContext  {printf("%3d: Contexto vacio\n", line_number);}              
                |TBREAK TSEMICOLON  {printf("%3d: Break sin contexto\n", line_number);}              
                |TCONTINUE TSEMICOLON {printf("%3d: Continue sin contexto\n", line_number);}                                 
                |execution  {printf("%3d: Execucion sin contexto\n", line_number);}                                               
                |TRETURN expresion TSEMICOLON {printf("%3d: Retorno sin contexto\n", line_number);}                
                |TBracketsOpen contextContent TBracketsClose  {printf("%3d: Contexto siin retorno\n", line_number);}                                
                |TBracketsOpen contextContent TRETURN expresion TSEMICOLON TBracketsClose {printf("%3d: Contexto con contenido y retorno al final\n", line_number);}
                |TBracketsOpen contextContent TRETURN expresion TSEMICOLON context TBracketsClose {printf("%3d: Contexto con contenido\n", line_number);}
                |TBracketsOpen TRETURN expresion TSEMICOLON TBracketsClose      {printf("%3d: Contexto con returno sin contenido\n", line_number);}                                

emptyContext: TBracketsOpen TBracketsClose {printf("%3d: Contexto vacio\n", line_number);}                                

contextContent: conditional    {printf("%3d: Condicional\n", line_number);} 
                |conditional contextContent {printf("%3d: Condicional\n", line_number);} 
                |whileLoop  {printf("%3d: While loop\n", line_number);}        
                |whileLoop contextContent {printf("%3d: While loop\n", line_number);}
                |doWhileLoop  {printf("%3d: Do while loop\n", line_number);}
                |doWhileLoop contextContent {printf("%3d: Do while loop\n", line_number);}
                |forLoop  {printf("%3d: For loop\n", line_number);}
                |forLoop contextContent {printf("%3d: For loop\n", line_number);}
                |switchConditional  {printf("%3d: Swich case\n", line_number);}
                |switchConditional contextContent {printf("%3d: Swich case\n", line_number);}
                |programContent
                |programContent contextContent
                |TBREAK TSEMICOLON {printf("%3d: TOKEN BREAK\n", line_number);} 
                |TCONTINUE      TSEMICOLON {printf("%3d: TOKEN CONTINUE\n", line_number);}                                                               



importLibrary:  TIMPORT TSTRINGVALUE TSEMICOLON {printf("%3d: Libreria\n", line_number);}                

comentary:  TONELINECOMMENTARY {printf("%3d: Comentario de una linea\n", line_number);}
            |TMULTIPLELINECOMMENTARY {printf("%3d: Comentario de multiples lienas\n", line_number);}

declaration:    variableDeclaration TSEMICOLON {printf("%3d: Declaracion de una variable\n", line_number);}                                                  

variableDeclaration:    variableDefinition {printf("%3d: Definicion de una variable\n", line_number);}
                        |variableDefinitionInitialized  {printf("%3d: Definicion de una variable pero inicializada\n", line_number);} 
                        |inmutability variableDefinition  {printf("%3d: Definicion de una variable pero con tipo de inmutabilidad\n", line_number);} 
                        |inmutability variableDefinitionInitialized {printf("%3d: Definicion de una variable inicializada pero con tipo de inutabilidad\n", line_number);}                        

variableDefinition:     valueType multipleVariablesDefinition {printf("%3d: Definicion de variable\n", line_number);}
                        |multipleVariablesDefinition {printf("%3d: Definicion de multiples variables en una sola linea\n", line_number);} 

multipleVariablesDefinition:    TIDENTIFIER {printf("%3d: Identificador\n", line_number);}
                                |TIDENTIFIER TCOMMA multipleVariablesDefinition {printf("%3d: Definicion de multiples variables\n", line_number);}                                                                                                

variableDefinitionInitialized:  variableDefinition variableAssignment   {printf("%3d: Inicializacion de una variable\n", line_number);}                                                                                                
                                
                              
variableAssignment:     TASSIGNMENT ternaryOperation    {printf("%3d: Asignacion de un valor a una variable\n", line_number);}                                  
                        |TASSIGNMENT ternaryOperation variableAssignment        {printf("%3d: Asignacion encadadenada de valores\n", line_number);}                                  

ternaryOperation:       expresion {printf("%3d: Expresion\n", line_number);}
                        |expresion TASKING ternaryOperation TCOLON ternaryOperation {printf("%3d: Expresion con operador ternario\n", line_number);}
                        |expresion TASKING operation TCOLON operation {printf("%3d: Expresion con operador ternario\n", line_number);}                                                                               
                        
operation:      TParenthesesOpen ternaryOperation TParenthesesClose {printf("%3d: Operacion de evaluacion\n", line_number);}

expresion:      value {printf("%3d: Valor\n", line_number);}
                |variableValue TADDITION TADDITION {printf("%3d: Post-incremento de valor\n", line_number);}
                |TADDITION TADDITION variableValue {printf("%3d: Pre-incremento de valor\n", line_number);}
                |variableValue TSUBSTRACTION TSUBSTRACTION {printf("%3d: Post-decremento de valor\n", line_number);}
                |TSUBSTRACTION TSUBSTRACTION variableValue {printf("%3d: Pre-decremento de valor\n", line_number);}
                |value operator expresion {printf("%3d: evaluacion con operadores\n", line_number);}                                                                      


value: variableValue {printf("%3d: Valor de variable\n", line_number);}
        |TNEGATION variableValue {printf("%3d: Negacion de valor de variable\n", line_number);}        
        |variableValue aritmeticOperation value {printf("%3d: Operacion aritmentica\n", line_number);}

variableValue:  TINTEGERVALUE {printf("%3d: Valor de tipo entero\n", line_number);}                                                                
                |TDOUBLEVALUE {printf("%3d: Valor de tipo double\n", line_number);}                                                                
                |TSTRINGVALUE {printf("%3d: Valor de tipo string \n", line_number);}                                                                
                |TBOOLVALUE {printf("%3d: Valor de tipo bool\n", line_number);}                                                                 
                |TIDENTIFIER  {printf("%3d: Identificador\n", line_number);}                                                                
                |functionConstructor  {printf("%3d: Constructor de una funcion\n", line_number);}                                                                
                |TIDENTIFIER classProperty  {printf("%3d: Propiedad de una clase\n", line_number);}
                |TNEW functionConstructor {printf("%3d: Constructor de una clase\n", line_number);}
                |getIndex {printf("%3d: Indice de una variable\n", line_number);}                   

getIndex: TIDENTIFIER TOPENINDEX TINTEGERVALUE TCLOSEINDEX {printf("%3d: Acceso al indice de una variable por entero\n", line_number);}
          |TIDENTIFIER TOPENINDEX TSTRINGVALUE TCLOSEINDEX {{printf("%3d: Indice de una variable por llave\n", line_number);}}

aritmeticOperation:     TMULTIPLICATION {printf("%3d: TOKEN MULTIPLICACION\n", line_number);}
                        |TDIVISION  {printf("%3d: TOKEN DIVISION\n", line_number);}
                        |TSUBSTRACTION  {printf("%3d: TOKEN SUBSTRACCION\n", line_number);}
                        |TADDITION  {printf("%3d: TOKEN ADICION\n", line_number);}

functionDeclaration:    anonimusFunction functionContent  {printf("%3d: Definicion de funcion anonima\n", line_number);}                                  
                        |TVOID functionConstructor functionContent {printf("%3d: Definicion de funcion de tipo void\n", line_number);}                                    
                        |valueType functionConstructor functionContent  {printf("%3d: Definicion de funcion con tipo\n", line_number);}                                  
                        |functionConstructor functionContent    {printf("%3d: Constructor de una funcion\n", line_number);}                                                                                                  
execution:  functionExecution TSEMICOLON {printf("%3d: Ejecucion de una funcion\n", line_number);}
                

functionExecution:      TIDENTIFIER classProperty  {printf("%3d: Ejecucion  de una funcion dentro de una clase\n", line_number);}                                                          
                        |functionConstructor

functionConstructor:    TIDENTIFIER anonimusFunction    {printf("%3d: Constructor de una funcion\n", line_number);}                                                              

functionContent:        onelineFunctionContent TSEMICOLON       {printf("%3d: Contenido de una sola linea par una funcion \n", line_number);}                                   
                        |context   {printf("%3d: Contenido de mas de una linea para una funcion\n", line_number);}                                                          

anonimusFunction :      TParenthesesOpen arguments TParenthesesClose    {printf("%3d: Funcion anonima con arguments\n", line_number);} 
                        |TParenthesesOpen  TParenthesesClose {printf("%3d: Funcion anonima sin arguments\n", line_number);}

onelineFunctionContent :TFATARROW operation   {printf("%3d: Operacion para una funcion de una sola linea\n", line_number);}                        
                        |TFATARROW operation variableAssignment {printf("%3d: Operacion para una funcion de una sola linea\n", line_number);}
                        |TFATARROW functionExecution {printf("%3d: Ejecucion de una funcion desde una funcion de una sola linea \n", line_number);}



arguments:      ternaryOperation     {printf("%3d: Argumentos de una funcion\n", line_number);} 
                |valueType TIDENTIFIER  {printf("%3d: Argumentos de una funcion\n", line_number);} 
                |TIDENTIFIER TCOMMA arguments   {printf("%3d: Argumentos de una funcion\n", line_number);} 
                |valueType TIDENTIFIER TCOMMA arguments {printf("%3d: Argumentos de una funcion\n", line_number);}                

valueType:      TINTEGER        {printf("%3d: TOKEN INTEGER\n", line_number);}                 
                |TDOUBLE        {printf("%3d: TOKEN DOUBLE\n", line_number);}
                |TNUMBER        {printf("%3d: TOKEN NUMBER\n", line_number);}
                |TBOOL  {printf("%3d: TOKEN BOOL\n", line_number);}
                |TSTRING        {printf("%3d: TOKEN STRING\n", line_number);}
                |TDYNAMIC       {printf("%3d: TOKEN DYNAMIC\n", line_number);} 
                |TVAR   {printf("%3d: TOKEN VAR\n", line_number);}
                |TList TLESSTHAN valueType TGREATERTHAN  {printf("%3d: Variable de tipo lista\n", line_number);}
                |TIDENTIFIER                       

classDefinition:TCLASS TIDENTIFIER TBracketsOpen TBracketsClose {printf("%3d: Definicion de una clase vacia\n", line_number);}                
                |TCLASS TIDENTIFIER TBracketsOpen classContent TBracketsClose   {printf("%3d: Definicion de una clase con contenido \n", line_number);}                
                |classType TCLASS TIDENTIFIER TBracketsOpen TBracketsClose      {printf("%3d: Definicion de una clase con tipo\n", line_number);}                
                |classType TCLASS TIDENTIFIER TBracketsOpen classContent TBracketsClose {printf("%3d: Definicion de una clase con tipo y contenido\n", line_number);}
                
                |TCLASS TIDENTIFIER TEXTENDS TIDENTIFIER TBracketsOpen TBracketsClose {printf("%3d: Definicion de una clase vacia con herencia\n", line_number);}                
                |TCLASS TIDENTIFIER TEXTENDS TIDENTIFIER TBracketsOpen classContent TBracketsClose   {printf("%3d: Definicion de una clase con contenido y con herencia\n", line_number);}                
                |classType TCLASS TIDENTIFIER TEXTENDS TIDENTIFIER TBracketsOpen TBracketsClose      {printf("%3d: Definicion de una clase con tipo y con herencia\n", line_number);}                
                |classType TCLASS TIDENTIFIER TEXTENDS TIDENTIFIER TBracketsOpen classContent TBracketsClose {printf("%3d: Definicion de una clase con tipo, contenido y herencia \n", line_number);}                                

classType:      TPRIVATE        {printf("%3d: Tipo de clase privada\n", line_number);}
                |TPROTECTED     {printf("%3d: Tipo de clase protegida\n", line_number);}
                |TPUBLIC        {printf("%3d: Tipo de clase publica\n", line_number);}
                
classContent:   declaration     {printf("%3d: Declaracion de una variable dentro de una clase\n", line_number);}
                |declaration classContent       {printf("%3d: Declaracion de una variable dentro de una clase\n", line_number);}
                |comentary  {printf("%3d: Comentario\n", line_number);} 
                |comentary classContent {printf("%3d: Comentario\n", line_number);} 
                |functionDeclaration    {printf("%3d: Delcaracion de una funcion dentro de una clase\n", line_number);}
                |functionDeclaration classContent       {printf("%3d: Contenido de una clase dentro de una clase\n", line_number);}
                |classDefinition        {printf("%3d: Definicion de una clase dentro de una clase\n", line_number);}
                |classDefinition classContent   {printf("%3d: Definicion de una clase con contenido\n", line_number);}

classProperty:  TDOT TIDENTIFIER     {printf("%3d: Propiedad de una clase\n", line_number);}
                |TDOT functionConstructor {printf("%3d: Propiedad de una clase de tipo funcion\n", line_number);}
                |TDOT TIDENTIFIER getIndex {printf("%3d: Propiedad de una clase con indice\n", line_number);}

classInstance:  TNEW anonimusFunction TSEMICOLON {printf("%3d: Instancia de una clase\n", line_number);}                

inmutability:   TCONSTANT       {printf("%3d: Tipo de inmutabilidad constante\n", line_number);}                
                |TFINAL {printf("%3d: Tipo de inmutabilidad final\n", line_number);}                

operator:       TCOMPARISON     {printf("%3d: Operador de comparacion\n", line_number);}
                |TDIFFERENT     {printf("%3d: Operador de diferencia\n", line_number);}
                |TLESSTHAN      {printf("%3d: Operador menor que\n", line_number);}
                |TLESSOREQUALTHAN       {printf("%3d: Operador menor o igual que\n", line_number);}
                |TGREATERTHAN   {printf("%3d: Operador mayor que\n", line_number);}
                |TGREATEROREQUALTHAN    {printf("%3d: Operador mayor o igual que\n", line_number);}                

conditional:    ifConditional {printf("%3d: Condicional if\n", line_number);}
                |ifConditional elseConditional  {printf("%3d: Condicional if con else\n", line_number);}                   
                |ifConditional multipleElseIfConditional elseConditional {printf("%3d: Condicional if con else if con else\n", line_number);}

ifConditional:  TIF meetCondition {printf("%3d: Condicional if\n", line_number);}


elseConditional:        TELSE context {printf("%3d: Condicional else\n", line_number);}
                        |TELSE declaration {printf("%3d: Condicional else\n", line_number);}                        

elseIfConditional:      TELSEIF meetCondition {printf("%3d: Condicional else if\n", line_number);}
                        

multipleElseIfConditional:      elseIfConditional {printf("%3d: Condicional else if\n", line_number);}
                                |elseIfConditional multipleElseIfConditional {printf("%3d: Multiples condicionales else if\n", line_number);}

meetCondition:  operation context {printf("%3d: Cumplimiento de una condicion\n", line_number);}
                |operation declaration  {printf("%3d: Cumplimiento de una condicion\n", line_number);}                

switchConditional: TSWICH operation TBracketsOpen cases TBracketsClose {printf("%3d: Switch case\n", line_number);}  

cases: TCASE variableValue TCOLON context {printf("%3d: case\n", line_number);}
      |TCASE variableValue TCOLON context cases {printf("%3d: multiples case\n", line_number);}

whileLoop: TWHILE meetCondition {printf("%3d: While loop\n", line_number);}

doWhileLoop:    TDO context TWHILE operation TSEMICOLON {printf("%3d: Do while loop\n", line_number);}                             

forLoop: TFOR TParenthesesOpen variableDefinitionInitialized  TSEMICOLON expresion  TSEMICOLON expresion TParenthesesClose context {printf("%3d: For loop\n", line_number);}        

%%                                                                                   
 
 #include <stdio.h>
 void yyerror(char const *s)
 {
 fprintf(stderr,"%s\n",s);
 }


int main ()                                                                              
{                                                                                    
  yyparse ();                                                                        
}
