//PATRONES DE DISEÑO FACTORY Y FACADE

import "dartisto:io"; //importacion de librerias
import "./example.dart";

class Object{ //definicion de una clase vacia 

}

/*comentarios de multiples lineas
 no acepta todos los caracteres
  acepta los tokens definidos como texto plano
*/

/*
definicion de clases con herencia
class PlotterPrinter extends Object
*/

class PlotterPrinter extends Object{
    
    Object object = new Object(); //instancia de clases 
   

    //ejecucion de funciones de una sola linea usando el operador "=>"

   void print(String nombre_archivo) => object.print(nombre_archivo);
    
    //declaraciones de funciones tipadas con o sin argumentos
    int show()
    {
        //asignacion de variables usando funciones y propiedades dentro de clases
        int quantity = object.numberTrabajoEsperaPlotter();
        
        //retorno en valores en contextos          
        return quantity;
    }   
    
    //declaraciones de funciones sin tipo con o sin arguments
    cleanQueue(true) => object.LimpiarPrintingQueuePlotter();    
}

//definicion de tipos de clases 
public class CashRegister extends Object{
    
    //inmutabilidad de variables
    
    /* variable de tipo final acepta un valor en timpo de ejecucion pero 
        ese valor una vez asignado quedara como constante
        
        variable de tipo const
    */

     final Object object; 
     object = Object(); //instancia de clases sin usar "new"

    //funciones sin retorno
    void open() => object.AbreCajaRegistradora();
    
    //funciones con tipo y retorno
    float showTotal()
    {
        //el retorno puede ser una ejecucion de una funcion
        return object.totalAPagar();             
    }

    //ejecucion de una sola linea que ejecuta a otra funcion
    setTotal(float quantity) => object.EstableceMontoAPagar(quantity);
   
}

private class Device{

    //funciones que aceptan multiples argumentos
    //concatenados por coma
    int selectDevice(int option,String a,b){

        //asignacion encadenada
        z=a=b; 

        //evaluacion de expresiones
        //condicional if
        if(option == 1)
        {   
            //multiples contextos anidados
            if(option == 1)
            return new PlotterPrinter();
            else return new PlotterPrinter(); //contextos de una sola linea
        }
        
        else if(option == 2){ //multiples else if
            return new PapperPrinter();
        }
        else if(option == 3){
            return new CashRegister();
        }                
        else
        {
            return CashRegister();
        }
    }

}


int main()
{
    Device manager = new Device();

    List<Object> devices; //declaracion de listas
    
    int option; //declaraciones sin inicializacion             
    
    //estructura while
    //negacion de valores
    while(!false)
    {
        //ejecucion de funciones
        print("Seleecione una opcion \n");
        print("1) Plotter \n");
        print("2) Printer \n");
        print("3) Cash Register \n");
        print("0) Salir \n");

        
        
        option = input();

        //estructura swich case
        switch(option)
        {
            case 0:{break;}
            case 11:break; 
        }
        
        //funciones y propiedades de clase como parametro de funciones
        devices.push(manager.selectDevice(option));

        //operaciones como argumentos
        print("cantidad de instancias: "+devices.length);        
    }
    
    //indices con llave
    print(devices["Plotter"]);
    
    //indices por valor entero
    print(devices[1]);

    //for loop
    for(int i=0; i<devices.length; i+1)
        devices.remove(i);
        
        //retorno de valores en funciones    
    return 1;    
    
}