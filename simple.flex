%{                                                                                           
#include "simple.tab.h"                                                                     
extern int line_number;                                                                     
%}                                                                                          

%option noyywrap                                                                            
 
%%

"int"   {return TINTEGER;}
"double"    {return TDOUBLE;}
"num"   {return TNUMBER;}

"String"    {return TSTRING;}
"void"  {return TVOID;}
"bool"  {return TBOOL;}

"public"    {return TPUBLIC;}
"private"   {return TPRIVATE;}
"protected" {return TPROTECTED;}

"dynamic"   {return TDYNAMIC;}
"var"   {return TVAR;}

"const" {return TCONSTANT;}
"final" {return TFINAL;}

"List"  {return TList;}

"class" {return TCLASS;}
"extends"   {return TEXTENDS;}
"new" {return TNEW;}
"return"    {return TRETURN;}

"if"    {return TIF;}
"else if"   {return TELSEIF;}
"else"  {return TELSE;}

"switch"    {return TSWICH;}
"case"  {return TCASE;}

"break" {return TBREAK;}
"continue" {return TCONTINUE;}

"this"  {return TTHIS;}
"super" {return TSUPER;}

"for" {return TFOR;}

"while" {return TWHILE;}
"do" {return TDO;}

"import" {return TIMPORT;}

[;] {return TSEMICOLON;}                                                                    
													
"=>"    {return TFATARROW;}

"=="    {return TCOMPARISON;}

[+]     {return TADDITION;}

[-]     {return TSUBSTRACTION;}
[/]     {return TDIVISION;}
[*]     {return TMULTIPLICATION;}

[=] {return TASSIGNMENT;}

"!" {return TNEGATION;}

"!="    {return TDIFFERENT;}
[<] {return TLESSTHAN;}
"<="    {return TLESSOREQUALTHAN;}
[>] {return TGREATERTHAN;}
">="    {return TGREATEROREQUALTHAN;}

"&&"    {return TAND;}
"||"    {return TOR;}

[?] {return TASKING;}
[:] {return TCOLON;}
[,] {return TCOMMA;}
[(] {return TParenthesesOpen;}
[)] {return TParenthesesClose;}
[{] {return TBracketsOpen;}
[}] {return TBracketsClose;}
"[" {return TOPENINDEX;}
"]" {return TCLOSEINDEX;}
[.] {return TDOT;}
[0-9][0-9]* {return TINTEGERVALUE;}

[0-9][0-9]*[.][0-9][0-9]*   {return TDOUBLEVALUE;}

["][^\r].*["]    {return TSTRINGVALUE;}

"true" | "false"    {return TBOOLVALUE;}

[_a-zA-Z][_a-zA-Z0-9]*  {return TIDENTIFIER; }

[/][/][^\r].* {return TONELINECOMMENTARY;}

[/][*][\n _\ta-zA-Z0-9]*[*][/]  {return TMULTIPLELINECOMMENTARY;}


[\n]    { line_number++; }                                                                     


%%
